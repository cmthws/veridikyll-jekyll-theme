# coding: utf-8

Gem::Specification.new do |spec|
  spec.name          = "veridikyll-jekyll-theme"
  spec.version       = "1.0.1"
  spec.authors       = ["Cary Mathews"]
  spec.email         = ["cmthws@outlook.com"]

  spec.summary       = %q{Adaption of TEMPLATED's Veridical theme to Jekyll}
  spec.homepage      = "https://gitlab.com/cmthws/veridikyll-jekyll-theme"
  spec.license       = "CC BY 3.0"

  spec.files         = `git ls-files -z`.split("\x0").select { |f| f.match(%r{^(assets|_layouts|_includes|_sass|LICENSE|README)}i) }

  spec.add_runtime_dependency "jekyll", "~> 3.4"

  spec.add_development_dependency "bundler", "~> 1.12"
  spec.add_development_dependency "rake", "~> 10.0"
end
