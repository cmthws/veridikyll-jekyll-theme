---
# You don't need to edit this file, it's empty on purpose.
# Edit theme's home layout instead if you wanna make some changes
# See: https://jekyllrb.com/docs/themes/#overriding-theme-defaults
layout: home
title: Homepage
navmenu: 1
---

<div id="wrapper1">
	<div id="welcome" class="container">
		<div class="title">
			<h2>Welcome to our website</h2>
			<span class="byline">Mauris vulputate dolor sit amet nibh</span> </div>
		<div class="content">
			<p>This is <strong>Veridikyll</strong>, a free, fully standards-compliant CSS template designed by <a href="http://templated.co" rel="nofollow">TEMPLATED</a> and ported to a <a href="https://jekyllrb.com">Jekyll</a> gem-based theme by <a href="https://gitlab.com/cmthws/">cmthws</a>. The photos in this template are from <a href="http://fotogrph.com/"> Fotogrph</a>. This free template is released under the <a href="http://templated.co/license">Creative Commons Attribution</a> license, so you're pretty much free to do whatever you want with it (even use it commercially) provided you give us credit for it. Have fun :) </p>
			<a href="#" class="button">Etiam posuere</a> </div>
	</div>
</div>
<div id="wrapper3">
	<div id="portfolio" class="container">
		<div class="title">
			<h2>Design Portfolio</h2>
			<span class="byline">Integer sit amet pede vel arcu aliquet pretium</span> </div>
		<div class="pbox1">
			<div class="column1">
				<div class="box"> <span class="icon icon-comments"></span>
					<h3>Vestibulum venenatis</h3>
					<p>Fermentum nibh augue praesent a lacus at urna congue rutrum.</p>
				</div>
			</div>
			<div class="column2">
				<div class="box"> <span class="icon icon-cogs"></span>
					<h3>Praesent scelerisque</h3>
					<p>Vivamus fermentum nibh in augue praesent urna congue rutrum.</p>
				</div>
			</div>
			<div class="column3">
				<div class="box"> <span class="icon icon-coffee"></span>
					<h3>Donec dictum metus</h3>
					<p>Vivamus fermentum nibh in augue praesent urna congue rutrum.</p>
				</div>
			</div>
			<div class="column4">
				<div class="box"> <span class="icon icon-cloud"></span>
					<h3>Mauris vulputate dolor</h3>
					<p>Rutrum fermentum nibh in augue praesent urna congue rutrum.</p>
				</div>
			</div>
		</div>
		<div class="pbox2">
			<div class="column1">
				<div class="box"> <span class="icon icon-asterisk"></span>
					<h3>Rhoncus volutpat</h3>
					<p>Fermentum nibh augue praesent a lacus at urna congue rutrum.</p>
				</div>
			</div>
			<div class="column2">
				<div class="box"> <span class="icon icon-headphones"></span>
					<h3>Sed odio sagittis</h3>
					<p>Vivamus fermentum nibh in augue praesent urna congue rutrum.</p>
				</div>
			</div>
			<div class="column3">
				<div class="box"> <span class="icon icon-user"></span>
					<h3>Aenean elementum</h3>
					<p>Vivamus fermentum nibh in augue praesent urna congue rutrum.</p>
				</div>
			</div>
			<div class="column4">
				<div class="box"> <span class="icon icon-signal"></span>
					<h3>Etiam posuere augue</h3>
					<p>Rutrum fermentum nibh in augue praesent urna congue rutrum.</p>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="wrapper2">
	<div id="featured" class="container">
		<div class="box1">
			<h2><span class="icon icon-group"></span>Fusce ultrices fringilla</h2>
			<p>Aliquam erat volutpat. Pellentesque tristique ante ut risus. Quisque dictum. Integer nisl risus, sagittis convallis, rutrum id, elementum congue, nibh. Suspendisse dictum porta lectus. Donec placerat odio vel elit. Nullam ante orci, pellentesque eget, tempus quis, ultrices in, est. Curabitur sit amet nulla. Donec leo, vivamus fermentum nibh in augue praesent a lacus at urna congue rutrum.</p>
		</div>
		<div class="box2">
			<h2><span class="icon icon-briefcase"></span>Etiam posuere augue</h2>
			<p>Aliquam erat volutpat. Pellentesque tristique ante ut risus. Quisque dictum. Integer nisl risus, sagittis convallis, rutrum id, elementum congue, nibh. Suspendisse dictum porta lectus. Donec placerat odio vel elit. Nullam ante orci, pellentesque eget, tempus quis, ultrices in, est. Curabitur sit amet nulla. Donec leo, vivamus fermentum nibh in augue praesent a lacus at urna congue rutrum.</p>
		</div>
	</div>
</div>
