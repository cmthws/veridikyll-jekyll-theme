# veridikyll-jekyll-theme

This theme is an adaption of the free (Creative Commons-licensed) Veridical website theme created by TEMPLATED (https://templated.co) to a gem-based theme for the Jekyll (https://jekyllrb.com) static site generator.

## Installation

Add this line to your Jekyll site's `Gemfile` to install from RubyGems.org:

```ruby
gem "veridikyll-jekyll-theme"
```

or to install it directly from gitlab:

```ruby
gem "veridikyll-jekyll-theme", :git => "https://cmthws@gitlab.com/cmthws/veridikyll-jekyll-theme.git"
```
And add this line to your Jekyll site's `_config.yml`:

```yaml
theme: veridikyll-jekyll-theme
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install veridikyll-jekyll-theme


## Usage

Current layouts are:

* _layouts/default.html
* _layouts/page.html : This is a 1-column page with a standard header; YAML Front Matter can include a page title (`title`) and a sub-title (`byline`) for inclusion in the rendered page.


Includes which can be overridden are:

* _includes/head.html : Insert third-party elements to go in the `<head>` section of the file here
* _includes/logo.html : Displayed on home page
* _includes/copyright.html : copyright portion of the bottom section of the site
	

The navigation menu is built at the time of site generation. For pages which should be added to the navigation menu, add the following variables to the YAML front matter:

```yaml
navmenu: <N>
title: <page title>
```

Where `<N>` is an integer indicating the order the page will appear in the navigation menu and `<page title>` will be used in the menu verbatium.

Site-wide keywords can be included in the rendered `<head>` section via the "site.keywords" variable. If a page (or post) has "categories" defined in its YAML Front Matter, those will be included rather than the site keywords.

## Contributing

Bug reports and pull requests are welcome on GitLab at https://gitlab.com/cmthws/veridikyll-jekyll-theme/issues. This is a website theme project: contributions, pull requests, and suggestions for improving the theme are welcomed from anyone.

## Development

To set up your environment to develop this theme, run `bundle install`.

Your theme is setup just like a normal Jekyll site! To test your theme, run `bundle exec jekyll serve` and open your browser at `http://localhost:4000`. This starts a Jekyll server using your theme. Add pages, documents, data, etc. like normal to test your theme's contents. As you make modifications to your theme and to your content, your site will regenerate and you should see the changes in the browser after a refresh, just like normal.

When your theme is released, only the files in `_layouts`, `_includes`, and `_sass` tracked with Git will be released.

## Versions

This project will use Semantic versioning (http://semver.org/) within the gemspec file to help identify changes. The following versions will be the guidelines for version numbers:

* Patch versions (N.x.**Y**) will be changes to existing files: adding/deleting code, moving code from one file to another, etc. The gemspec version will not be updated for changes to non-theme related files (i.e. README, LICENSE, gemspec, etc.) 
* Minor versions (N.**X**.y) will be for the creation or deletion of files within the theme, such as `_layout`, `_includes`, or `asset` file (for example). The gemspec version will not be updated for non-theme related files.
* Major versions (**N**.x.y) will be as follows:
 * 0.x.y - initial porting effort
 * 1.x.y - changes which maintain compatibility with TEMPLATED's original design (e.g., CSS properties). Barring small text content changes, 1.x versions of Veridikyll should render the same as the original Veridical site theme.
 * 2.x.y - changes which break compatibility with TEMPLATED's original design. Verdikyll 2.x versions will differ from the default properties offered by Veridical.

## License

The theme is available as open source under the terms of the [Creative Commons Attribution 3.0 Unported](http://creativecommons.org/licenses/by/3.0/) license.

